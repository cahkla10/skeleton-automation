package setups;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by madeboga on 9/13/17.
 */
public class DriverSetup extends DriverPool {
    private AppiumDriverLocalService service;
    public final static boolean isAndroid = false;
    private final static boolean isCloud = false;
    private final static boolean isRealDevice = false;
    private PropertiesSetup proper;
    private Properties envProp;

    public DriverSetup() {
        proper = new PropertiesSetup();
        envProp = proper.getEnvProperties();
    }


    protected void prepareAutomation() throws MalformedURLException {
        File appDir = new File(proper.property);
        File app = (isAndroid) ? new File(appDir, envProp.getProperty("apk")) : isRealDevice ? new File(appDir, envProp.getProperty("ipa")) : new File(appDir, envProp.getProperty("app"));
        Capabilities capabilities = new Capabilities();
        DesiredCapabilities desiredCapabilities;
        if (isCloud)
            desiredCapabilities = isAndroid ? capabilities.CloudAndroidCapabilities() : capabilities.CloudIOSCapabilities();
        else
            desiredCapabilities = isAndroid ? capabilities.LocalAndroidCapabilities(isRealDevice) : capabilities.LocalIOSCapabilities(isRealDevice);
        desiredCapabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
        setDriver(desiredCapabilities);
    }

    private void setDriver(DesiredCapabilities capabilities) throws MalformedURLException {
        AppiumDriver<WebElement> driver;
        if (isCloud)
            driver = isAndroid ? new AndroidDriver(new URL(envProp.getProperty("cloudURL")), capabilities) : new IOSDriver(new URL(envProp.getProperty("cloudURL")), capabilities);
        else
            driver = isAndroid ? new AndroidDriver(new URL(envProp.getProperty("localURL")), capabilities) : new IOSDriver(new URL(envProp.getProperty("localURL")), capabilities);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        setDriverInstance(driver);
    }

    protected void startAppium(){
        service = AppiumDriverLocalService.buildDefaultService();
        service.start();
    }
    protected void stopAppium(){
        service.stop();
    }
}
