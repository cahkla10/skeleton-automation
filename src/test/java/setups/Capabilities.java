package setups;

import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by madeboga on 9/13/17.
 */
public class Capabilities {

    public DesiredCapabilities LocalAndroidCapabilities(boolean isRealDevice){
        String deviceNameAndroid;
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("device", "Android");
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
        capabilities.setCapability("platformName", "Android");
        deviceNameAndroid = isRealDevice ? "445e2f66" : "emulator-5554" ; /* Capabilities Using RealDevice */
        capabilities.setCapability("deviceName", deviceNameAndroid);
        return capabilities;
    }

    public DesiredCapabilities LocalIOSCapabilities(boolean isRealDevice){
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6s");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.3");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        if(isRealDevice) {
            /* Capabilities Using RealDevice */
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "CIMB");
            capabilities.setCapability(MobileCapabilityType.UDID,"e9717e98c86d3336ecd343fbd68400980ab9fca5");
            capabilities.setCapability("xcodeOrgId","RF5PH3T3P7");
            capabilities.setCapability("xcodeSigningId","iPhone Developer");
           /* Capabilities Using RealDevice */
        }
        return capabilities;
    }

    public DesiredCapabilities CloudAndroidCapabilities(){
        /*This cloud capabilities using Kobiton*/
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("sessionName", "Automation test session IOS");
        capabilities.setCapability("sessionDescription", "");
        capabilities.setCapability("deviceOrientation", "portrait");
        capabilities.setCapability("captureScreenshots", true);
        capabilities.setCapability("app", "kobiton-store:1077");
        capabilities.setCapability("deviceGroup", "KOBITON");
        capabilities.setCapability("deviceName", "Galaxy S6");
        capabilities.setCapability("platformVersion", "6.0.1");
        capabilities.setCapability("platformName", "Android");
        return capabilities;
    }

    public DesiredCapabilities CloudIOSCapabilities(){
        /*This cloud capabilities using Kobiton*/
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("sessionName", "Automation test session");
        capabilities.setCapability("sessionDescription", "");
        capabilities.setCapability("deviceOrientation", "portrait");
        capabilities.setCapability("captureScreenshots", true);
        capabilities.setCapability("app", "kobiton-store:1223");
        capabilities.setCapability("deviceGroup", "KOBITON");
        capabilities.setCapability("deviceName", "iPhone 7 Plus");
        capabilities.setCapability("platformVersion", "10.3.3");
        capabilities.setCapability("platformName", "iOS");

        return capabilities;
    }
}
