package setups;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by madeboga on 9/15/17.
 */
public class PropertiesSetup {

    public String property = System.getProperty("user.dir")+ "/src/test/resources/";

    public Properties getEnvProperties(){
        return getProperties(property+ "environment.properties");
    }

    private Properties getProperties(String propFile){
        Properties prop = new Properties();
        File file = new File(propFile);
        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            prop.load(fileInput);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}
