package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steps.FunctionalStep;

/**
 * Created by madeboga on 9/13/17.
 */
public class IOSHomePage extends FunctionalStep {

    @FindBy(id = "update_later_button")
    private WebElement btnLater;

    @FindBy(id = "update_now_button")
    private WebElement btnNow;

    @FindBy(id = "update_alert_view")
    private WebElement alertUpdate;

    @FindBy(xpath = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeNavigationBar[1]/XCUIElementTypeStaticText[1]")
    private WebElement lblHeader;

    @FindBy(id = "label_point_value")
    private WebElement lblPoint;

    public IOSHomePage() {
        PageFactory.initElements(getDriverInstance(),this);
    }

    public void tapBtnLater() throws Exception {
        btnLater.isDisplayed();
        btnLater.click();
    }
    public void tapBtnNow() throws Exception {
        btnNow.isDisplayed();
        btnNow.click();
    }
    public void validateAlertUpdateDisplayed(){
        alertUpdate.isDisplayed();
    }
    public void validateHomePageDisplayed(){
        lblHeader.isDisplayed();
        lblPoint.isDisplayed();
    }
}
