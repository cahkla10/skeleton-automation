package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steps.FunctionalStep;

/**
 * Created by madeboga on 9/13/17.
 */
public class AndroidHomePage extends FunctionalStep {
    @FindBy(id = "android:id/button2")
    private WebElement btnLater;

    @FindBy(id = "android:id/button1")
    private WebElement btnNow;

    @FindBy(id = "com.cimbniaga.lifestyleapp:id/alertTitle")
    private WebElement alertUpdate;

    @FindBy(id = "com.cimbniaga.lifestyleapp:id/toolbar")
    private WebElement lblHeader;

    @FindBy(id = "com.cimbniaga.lifestyleapp:id/cardLimit")
    private WebElement lblPoint;

    public AndroidHomePage() {
        PageFactory.initElements(getDriverInstance(),this);
    }

    public void tapBtnLater() throws Exception {
        btnLater.isDisplayed();
        btnLater.click();
    }
    public void tapBtnNow() throws Exception {
        btnNow.isDisplayed();
        btnNow.click();
    }
    public void validateAlertUpdateDisplayed(){
        alertUpdate.isDisplayed();
    }
    public void validateHomePageDisplayed(){
        lblHeader.isDisplayed();
        lblPoint.isDisplayed();
    }
}
