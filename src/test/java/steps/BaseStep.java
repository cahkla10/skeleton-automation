package steps;

import org.openqa.selenium.support.ui.WebDriverWait;
import setups.DriverSetup;

import java.net.MalformedURLException;

/**
 * Created by madeboga on 9/13/17.
 */
public class BaseStep extends DriverSetup {
    private static boolean initialized = false;

    public BaseStep(){
        super();
        if (!initialized) {
            WebDriverWait wait = new WebDriverWait(getDriverInstance(), 30);
            InitializedStep.setWaitInstance(wait);
        }else {
            try {
                prepareAutomation();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            initialized = true;
        }
    }
}
