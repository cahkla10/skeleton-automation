package steps;

import cucumber.api.java.en.Given;
import pages.AndroidHomePage;
import pages.IOSHomePage;
import setups.DriverSetup;

/**
 * Created by madeboga on 9/13/17.
 */
public class HomeStep {
    private AndroidHomePage androidHomePage;
    private IOSHomePage iosHomePage;

    public HomeStep() {
        androidHomePage = new AndroidHomePage();
        iosHomePage = new IOSHomePage();
    }

    @Given("^User tap on button (.*)$")
    public void userTapOnButtonButton(String button) throws Throwable {
        if (button.equalsIgnoreCase("later")) {
            if (DriverSetup.isAndroid) {
                androidHomePage.validateAlertUpdateDisplayed();
                androidHomePage.tapBtnLater();
                androidHomePage.validateHomePageDisplayed();
            }else {
                iosHomePage.validateAlertUpdateDisplayed();
                iosHomePage.tapBtnLater();
                iosHomePage.validateHomePageDisplayed();
            }
        }
        else if (button.equalsIgnoreCase("now")) {
            if (DriverSetup.isAndroid) {
                androidHomePage.validateAlertUpdateDisplayed();
                androidHomePage.tapBtnNow();
            }else {
                iosHomePage.validateAlertUpdateDisplayed();
                iosHomePage.tapBtnNow();
            }
        }
    }
}
