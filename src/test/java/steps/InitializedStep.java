package steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import setups.DriverSetup;

/**
 * Created by madeboga on 9/13/17.
 */
public class InitializedStep extends DriverSetup {

    @Before
    public void before() throws Exception {
        startAppium();
        prepareAutomation();
    }
    @After
    public void after() throws Exception {
        getDriverInstance().quit();
        stopAppium();
    }
}
